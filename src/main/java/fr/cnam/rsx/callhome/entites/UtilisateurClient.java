package fr.cnam.rsx.callhome.entites;

import java.util.HashSet;
import java.util.Set;

public class UtilisateurClient extends Utilisateur {

  /**
   * Pourqu'on puisse le transporter sur le reseau.
   */
  private static final long serialVersionUID = 1L;

  private Boolean isConnected = false;

  private Statistique statistique;

  private Set<String> codeErreurList = new HashSet<String>();

  private String codeActivation;

  private String motDePasseAdmin;

  private String cheminDuRapport;

  private String version;

  public Boolean getIsConnected() {
    return isConnected;
  }

  public void setIsConnected(Boolean isConnected) {
    this.isConnected = isConnected;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public Statistique getStatistique() {
    return statistique;
  }

  public void setStatistique(Statistique statistique) {
    this.statistique = statistique;
  }

  public void addErreur(String erreurCode) {
    codeErreurList.add(erreurCode);
  }

  public Set<String> getCodeErreurList() {
    return codeErreurList;
  }

  public String getCodeActivation() {
    return codeActivation;
  }

  public void setCodeActivation(String codeActivation) {
    this.codeActivation = codeActivation;
  }

  public String getMotDePasseAdmin() {
    return motDePasseAdmin;
  }

  public void setMotDePasseAdmin(String motDePasseAdmin) {
    this.motDePasseAdmin = motDePasseAdmin;
  }

  public String getCheminDuRapport() {
    return cheminDuRapport;
  }

  public void setCheminDuRapport(String cheminDuRapport) {
    this.cheminDuRapport = cheminDuRapport;
  }

  @Override
  public boolean equals(Object o) {
    boolean result = false;
    if (o instanceof UtilisateurClient) {
      UtilisateurClient that = (UtilisateurClient) o;
      result = (this.getUtilisateurId().equals(that.getUtilisateurId()));
    }
    return result;
  }

  @Override
  public int hashCode() {
    return this.getUtilisateurId().hashCode();
  }
}
