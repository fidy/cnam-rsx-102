package fr.cnam.rsx.callhome.entites;

import java.io.Serializable;

public abstract class Utilisateur implements Serializable {

  /**
   * Pourqu'on puisse le transporter sur le reseau.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Identifiant du client.
   */
  private String utilisateurId;

  /**
   * Référence au serveur auquel ce client est connécté
   */
  private ApplicationServeur serveur;

  /*
   * Si le client est un administrateur du serveur.
   */
  private Boolean estAdmin;

  public ApplicationServeur getServeur() {
    return serveur;
  }

  public void setServeur(ApplicationServeur serveur) {
    this.serveur = serveur;
  }

  public Boolean getEstAdmin() {
    return estAdmin;
  }

  public void setEstAdmin(Boolean estAdmin) {
    this.estAdmin = estAdmin;
  }

  public String getUtilisateurId() {
    return utilisateurId;
  }

  public void setUtilisateurId(String utilisateurId) {
    this.utilisateurId = utilisateurId;
  }

}
