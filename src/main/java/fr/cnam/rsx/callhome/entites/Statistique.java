package fr.cnam.rsx.callhome.entites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Toutes les statistiques du client.
 * 
 * @author Fidèle
 *
 */
public class Statistique implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String systemExploitation;
  private Double utilsationCPU;
  private Double utilisationMem;
  private Double utilisationDisque;
  private List<String> logicielsLesPlusUtilises = new ArrayList<String>();

  public String getSystemExploitation() {
    return systemExploitation;
  }

  public void setSystemExploitation(String systemExploitation) {
    this.systemExploitation = systemExploitation;
  }

  public Double getUtilsationCPU() {
    return utilsationCPU;
  }

  public void setUtilsationCPU(Double utilsationCPU) {
    this.utilsationCPU = utilsationCPU;
  }

  public Double getUtilisationMem() {
    return utilisationMem;
  }

  public void setUtilisationMem(Double utilisationMem) {
    this.utilisationMem = utilisationMem;
  }

  public Double getUtilisationDisque() {
    return utilisationDisque;
  }

  public void setUtilisationDisque(Double utilisationDisque) {
    this.utilisationDisque = utilisationDisque;
  }

  public List<String> getLogicielsLesPlusUtilises() {
    return logicielsLesPlusUtilises;
  }

  public void setLogicielsLesPlusUtilises(List<String> logicielsLesPlusUtilises) {
    this.logicielsLesPlusUtilises = logicielsLesPlusUtilises;
  }

}
