package fr.cnam.rsx.callhome.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ApplicationServeur {

  /**
   * Par défaut http.
   */
  private String protocole = "http";

  private String nomServeur;

  private long portServeur;

  public String getNomServeur() {
    return nomServeur;
  }

  public void setNomServeur(String nomServeur) {
    this.nomServeur = nomServeur;
  }

  public long getPortServeur() {
    return portServeur;
  }

  public void setPortServeur(long portServeur) {
    this.portServeur = portServeur;
  }

  public String getProtocole() {
    return protocole;
  }

  public void setProtocole(String protocole) {
    this.protocole = protocole;
  }

  /**
   * Reconstitue l'adresse du serveur.
   * 
   * @return adresse du serveur
   */
  @JsonIgnore
  public String getAdresseServeur() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.protocole);
    sb.append(":");
    sb.append(this.protocole.endsWith("//") ? "" : "//");
    sb.append(this.nomServeur);
    sb.append(":");
    sb.append(portServeur);
    return sb.toString();
  }
}
