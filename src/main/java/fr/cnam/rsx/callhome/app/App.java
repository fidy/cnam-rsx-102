package fr.cnam.rsx.callhome.app;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fr.cnam.rsx.callhome.entites.UtilisateurClient;

/**
 * 
 * @author Fidèle
 * 
 */
public class App {

  public static String nomServeur = null;
  public static int portServeur = 8080;

  public static String nomServeurRedirection = null;
  public static int portServeurRedirection = 0;

  private static final String SERVER_PARAMS_NAME = "server_parameters";

  public static String MOT_DE_PASSE_ADMIN = "admin";
  /**
   * Liste des applications clients qui sont connectées.
   */
  public static final Map<String, UtilisateurClient> clientsConnectes = new Hashtable<String, UtilisateurClient>();

  /**
   * Liste des applications clients licites authorisés à reçevoir des mises à jour.
   */
  public static final Map<String, UtilisateurClient> clientsLicites = new Hashtable<String, UtilisateurClient>();

  /**
   * Liste des codes activations clients licites afin de les authoriser à reçevoir des mises à jour.
   */
  public static final List<String> codesActivationsLicites = Arrays.asList("XYZ11", "XYZ22",
      "XYZ33", "XYZ44");

  /**
   * Liste de tous les clients qui ont envoyé des informations.
   */
  public static final Map<String, UtilisateurClient> clientsAvecInfo = new Hashtable<String, UtilisateurClient>();

  public static void main(String[] args) throws Exception {

    // Charger les proprietés du serveur à partir d'un fichier de config
    String serverConfigCheminFichier = System.getProperty(SERVER_PARAMS_NAME);
    Properties properties = new Properties();
    try {
      properties.load(new FileInputStream(serverConfigCheminFichier));

      nomServeur = properties.getProperty("server.nom");
      portServeur = Integer.valueOf(properties.getProperty("server.port"));

      nomServeurRedirection = properties.getProperty("server.redirection.nom");
      portServeurRedirection = Integer.valueOf(properties.getProperty("server.redirection.port"));

      App.log("Les configurations suivantes seront appliquées :");
      App.log("Nom du serveur : " + nomServeur);
      App.log("Numero de port du serveur : " + portServeur);
      App.log("Nom du serveur de redirection : " + nomServeurRedirection);
      App.log("Numero de port du serveur de redirection : " + portServeurRedirection);

    } catch (IOException e) {

      nomServeur = "localhost";
      portServeur = 8080;

      nomServeurRedirection = "localhost";
      portServeurRedirection = 8082;

      App.log("");
      App.log("Nom du serveur : " + nomServeur);
      App.log("Numero de port du serveur : " + portServeur);
      App.log("Nom du serveur de redirection : " + nomServeurRedirection);
      App.log("Numero de port du serveur de redirection : " + portServeurRedirection);
    }

    Server server = new Server(portServeur);

    ResourceConfig config = new ResourceConfig();
    config.packages("fr.cnam.rsx.callhome")
    // config.register(JacksonFeatures.class) //
    .register(MultiPartFeature.class);
    ServletHolder servlet = new ServletHolder(new ServletContainer(config));

    ServletContextHandler context = new ServletContextHandler(server, "/*");
    context.addServlet(servlet, "/*");
    try {
      server.start();
      server.join();
    }
    finally {
      server.destroy();
    }
  }

  /**
   * Un simple logger
   * 
   * @param message : message a afficher
   * @return message à afficher avec marqueur.
   */
  public static void log(String message) {
    StringBuilder sb = new StringBuilder("===== ");
    sb.append(message);
    System.out.println(sb.toString());
  }
}
