package fr.cnam.rsx.callhome.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import fr.cnam.rsx.callhome.app.App;
import fr.cnam.rsx.callhome.entites.ApplicationServeur;
import fr.cnam.rsx.callhome.entites.Statistique;
import fr.cnam.rsx.callhome.entites.UtilisateurClient;

/**
 * 
 * @author Fidèle
 *
 */
@Path("home")
public class HomeController {

  private static final String SUCCESS = "success";

  @GET
  @Path("hello")
  @Produces(MediaType.TEXT_PLAIN)
  public String login() {
    return "Hello world";
  }

  @POST
  @Path("hello")
  @Consumes(MediaType.TEXT_PLAIN)
  @Produces(MediaType.TEXT_PLAIN)
  public String sayHello() {
    return "Hello";
  }

  @POST
  @Path("logout")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public String logout(UtilisateurClient clientConnecte) {

    // on retire le client parmis ceux qui sont connectés
    if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) != null) {
      App.clientsConnectes.remove(clientConnecte.getUtilisateurId());
    }

    // on retire le client parmis ceux qui sont licites
    if (App.clientsLicites.get(clientConnecte.getUtilisateurId()) != null) {
      App.clientsLicites.remove(clientConnecte.getUtilisateurId());
    }

    App.log("Le client '" + clientConnecte.getUtilisateurId() + "' vient de se déconnecter.\n");
    return SUCCESS;
  }

  @POST
  @Path("register")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public String enregistrerClient(UtilisateurClient clientConnecte) {
    // On range client dans la liste des clients connectés.
    if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) == null) {
      App.clientsConnectes.put(clientConnecte.getUtilisateurId(), clientConnecte);
      App.log("Le client '" + clientConnecte.getUtilisateurId() + "' vient de se connecter. \n");
    } else {
      App.log("Le client est déjà connnecté. \n");
    }
    return SUCCESS;
  }

  @POST
  @Path("redirect")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response redirecte(UtilisateurClient clientConnecte) {
    // On renvoie au client l'adresse du nouveau serveur
    ApplicationServeur serveur = new ApplicationServeur();
    serveur.setNomServeur(App.nomServeurRedirection);
    serveur.setPortServeur(App.portServeurRedirection);
    return Response.status(200).entity(serveur).build();
  }

  @POST
  @Path("checkupdate")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public Response testVersion(UtilisateurClient clientConnecte) {
    // version client
    String versionClient = clientConnecte.getVersion();
    String message = "";
    if ("10.1".equalsIgnoreCase(versionClient)) {
      message = "Votre version '" + clientConnecte.getVersion() + "' est à jour. \n";
    } else {
      message = "Votre version '" + clientConnecte.getVersion()
          + "' n'est pas à jour, la version à jour est la 10.1. \n";
    }
    return Response.status(200).entity(message).build();
  }

  @POST
  @Path("statistique")
  @Consumes(MediaType.APPLICATION_JSON)
  public void getStatistiqueClient(UtilisateurClient clientConnecte) {
    // Statistique du client
    Statistique stat = clientConnecte.getStatistique();
    App.log("STAT du client " + clientConnecte.getUtilisateurId());
    App.log("Utilisation Disque : " + stat.getUtilisationDisque() + "%");
    App.log("Utilisation Memoire : " + stat.getUtilisationMem() + "%");
    App.log("Utilisation CPU : " + stat.getUtilsationCPU() + "%");
    App.log("Logiciel le plus utilisées : " + stat.getLogicielsLesPlusUtilises().toString());
    System.out.println("\n");

    // mettre à jour ses statistique.
    if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) != null) {
      App.clientsConnectes.get(clientConnecte.getUtilisateurId()).setStatistique(stat);
    }

    // mettre dans la liste des clients qui ont envoyé des informations.
    App.clientsAvecInfo.put(clientConnecte.getUtilisateurId(), clientConnecte);
  }

  @POST
  @Path("uploadrapport")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.TEXT_PLAIN)
  public Response recevoirFichierRapport(@FormDataParam("file") InputStream uploadInputStream,
      @FormDataParam("file") FormDataContentDisposition detailsFichier,
      @FormDataParam("clientConnecte") String client) {
    String rep = System.getProperty("user.dir") + "\\";
    Gson gson = new GsonBuilder().create();
    UtilisateurClient clientConnecte = gson.fromJson(client, UtilisateurClient.class);
    // construire le nom du fichier a ranger
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String dateAujourdhui = sdf.format(new Date());
    String nomRapport = "rapport_" + clientConnecte.getUtilisateurId() + "_" + dateAujourdhui
        + ".txt";
    try {
      OutputStream out = new FileOutputStream(new File(rep + nomRapport));
      int read = 0;
      byte[] bytes = new byte[1024];
      while ((read = uploadInputStream.read(bytes)) != -1) {
        out.write(bytes, 0, read);
      }
      out.flush();
      out.close();

      clientConnecte.setCheminDuRapport(rep + nomRapport);
      // mettre dans la liste des clients qui ont envoyé des informations.
      App.clientsAvecInfo.put(clientConnecte.getUtilisateurId(), clientConnecte);
      System.out.println("Le rapport a été bien téléversé dans  " + rep + nomRapport);
      return Response.status(200).entity(rep + nomRapport).build();
    } catch (IOException e) {
      return Response.status(400).entity("ERROR").build();
    }
  }

  @POST
  @Path("reporterror")
  @Consumes(MediaType.APPLICATION_JSON)
  public void getClientError(UtilisateurClient clientConnecte) {
    // mettre à jour la lites des errors du client.
    if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) != null) {
      App.clientsConnectes.get(clientConnecte.getUtilisateurId()).getCodeErreurList().addAll(
          clientConnecte.getCodeErreurList());
    }

    // mettre dans la liste des clients qui ont envoyé des informations.
    App.clientsAvecInfo.put(clientConnecte.getUtilisateurId(), clientConnecte);
    App.log("Le client '" + clientConnecte.getUtilisateurId()
        + "' vient de rapporter une erreur de type " + clientConnecte.getCodeErreurList());
  }

  @POST
  @Path("active")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public Response activerVersion(UtilisateurClient clientConnecte) {
    // Récuperer le code activation envoyé par le client.
    String codeActivation = clientConnecte.getCodeActivation();
    String retour = "Votre code activation n'est pas valide";

    // mettre à jour code activation des errors du client, si le client est connecté
    if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) != null) {
      UtilisateurClient client = App.clientsConnectes.get(clientConnecte.getUtilisateurId());
      client.setCodeActivation(clientConnecte.getCodeActivation());
      if (App.codesActivationsLicites.contains(codeActivation)) {
        retour = "Votre code activation est valide. Vous reçevrez automatiquement les MAJ.";
        if (!App.clientsLicites.containsKey(client.getUtilisateurId())) {
          App.clientsLicites.put(client.getUtilisateurId(), client);
        }
      }
    }
    return Response.status(200).entity(retour).build();
  }

  @POST
  @Path("authentifieAdmin")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public Response authenticateAdmin(UtilisateurClient clientConnecte) {
    // comparer le mot de passe envoyé avec celui autorisé par le serveur
    boolean estAuthentifie = App.MOT_DE_PASSE_ADMIN.equals(clientConnecte.getMotDePasseAdmin());
    if (estAuthentifie) {
      if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) != null) {
        UtilisateurClient client = App.clientsConnectes.get(clientConnecte.getUtilisateurId());
        client.setMotDePasseAdmin(clientConnecte.getMotDePasseAdmin());
        client.setEstAdmin(true);
      }
      if (App.clientsLicites.containsKey(clientConnecte.getUtilisateurId())) {
        UtilisateurClient client = App.clientsLicites.get(clientConnecte.getUtilisateurId());
        client.setMotDePasseAdmin(clientConnecte.getMotDePasseAdmin());
        client.setEstAdmin(true);
      }
    }
    App.log("Le client '" + clientConnecte.getUtilisateurId()
        + "' vient de se connecter entant qu'admin. \n");
    return Response.status(200).entity(estAuthentifie).build();
  }

  @POST
  @Path("checkCodeActivation")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.TEXT_PLAIN)
  public Response testCodeActivation(UtilisateurClient clientConnecte) {
    // code activation dans le client
    String codeActivation = clientConnecte.getCodeActivation();
    String message = "KO";

    if (App.clientsConnectes.get(clientConnecte.getUtilisateurId()) != null) {
      UtilisateurClient client = App.clientsConnectes.get(clientConnecte.getUtilisateurId());
      client.setCodeActivation(clientConnecte.getCodeActivation());
      if (codeActivation != null && !codeActivation.isEmpty()
          && App.codesActivationsLicites.contains(codeActivation)) {
        message = "10.1";
      }
    }
    return Response.status(200).entity(message).build();
  }

  @GET
  @Path("listeClientInfo")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getListClientAvecInfo() {
    // rechercher tous les clients avec info
    Gson gson = new GsonBuilder().create();
    Collection<UtilisateurClient> clients = App.clientsAvecInfo.values();
    return Response.status(200).entity(gson.toJson(clients)).build();
  }

  @GET
  @Path("downloadrapport")
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  public Response downloadRapport(@QueryParam(value = "clientId") String clientId) {
    // Récupérer le client de la liste des clients ayant envoyé des infos.
    UtilisateurClient clientAvecInfo = App.clientsAvecInfo.get(clientId);
    String cheminFichier = "";
    if (clientAvecInfo != null && clientAvecInfo.getCheminDuRapport() != null) {
      cheminFichier = clientAvecInfo.getCheminDuRapport();
    }
    try {
      File f = new File(cheminFichier);
      final FileInputStream fStream = new FileInputStream(f);
      StreamingOutput stream = new StreamingOutput() {

        public void write(OutputStream output) throws IOException, WebApplicationException {
          try {
            int length;
            byte[] buffer = new byte[1024];
            while ((length = fStream.read(buffer)) != -1) {
              output.write(buffer, 0, length);
            }
            output.flush();
          } catch (Exception e) {
            throw new WebApplicationException(e);
          }
        }
      };
      return Response.ok(stream).build();
    } catch (Exception e) {
      return Response.status(Response.Status.CONFLICT).build();
    }
  }
}
